

module.exports = function (fuck) {
  return $(function() {
      console.log( "ready!" );
  });
}

$(function(){

  var shrinkHeader = 200,
      tl = TweenLite,
      isShrunk = false;

  var shrunkVar = function () {
    isShrunk ? isShrunk = false : isShrunk = true;
  }

  var shrink = function () {
    // tl.to('.bgImage', 1.25, {
    //   '-webkit-filter': 'grayscale(1)'
    // })
  
    $('.nav_Wrapper').addClass('shrunk')

    $('.splash_Container img').addClass('shrunk')
    shrunkVar();
  }

  var expand = function () {
    // tl.to('.bgImage', 1.25, {
    //   '-webkit-filter': 'grayscale(0)'
    // })

    $('.nav_Wrapper').removeClass('shrunk')

    $('.splash_Container img').removeClass('shrunk')
    shrunkVar();

  }

  $(window).scroll(function() {
    var scroll = getCurrentScroll();
      if ( scroll >= shrinkHeader ) {
          if (!isShrunk) {
            shrink();
          };
        }
        else {
          if (isShrunk) {
            expand();
          };
        }
  });

  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }
});